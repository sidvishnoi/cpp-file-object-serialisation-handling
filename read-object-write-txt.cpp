/*
MIT License

Copyright (c) 2017 Sudhanshu Vishnoi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*
Module for reading an object file, with objects of class Person
and write it's attributes to a text file

Usage:

include following for separate module testing:
#include "iostream"
#include "fstream"
#include "cstdlib"
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include "person-class.cpp"

$ g++ read-object-write-txt.cpp -lboost_serialization
$ ./a.out
*/

void read_object_write_text(char* infilename, char* outfilename) {
    /*
    Objective:  to read a binary object file (objects being of Person class)
        and re-create person object for each enrty and write output to a text file
    Input parameters:
        infilename : name of input object file
        outfilename: name of output text file
    Return: None
    Error:
        if input file cannot open, "Cannot open given file. EXITING." exit(0)
    Assumption: all entries in object file are serialized Person objects
    */
    std::ifstream infile(infilename);
    if (!infile) {
        std::cout << "Cannot open given file. EXITING.\n";
        exit(0);
    }
    std::ofstream outfile(outfilename);


    Person p;

    while (true) {
        try {
            // read serialized object to Person object
            boost::archive::binary_iarchive ia(infile);
            ia >> p;

            // write object attributes to outfile
            outfile << "Name: " << p.getName()
            << "\nDate of Birth: " << p.getDob()
            << "\nAddress: " << p.getAddress() << "\n=======\n";
        } catch (boost::archive::archive_exception) {
            break;
        }
    }

    infile.close();
    outfile.close();
}

// int main() {
//     char infile[100], outfile[100];
//     std::cout << "Path of input (object) file: ";
//     std::cin.getline(infile, sizeof(infile));
//     std::cout << "Path of output (csv/txt) file: ";
//     std::cin.getline(outfile, sizeof(outfile));

//     read_object_write_text(infile, outfile);
//     return 0;
// }
