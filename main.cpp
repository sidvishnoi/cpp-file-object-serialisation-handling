/*
MIT License

Copyright (c) 2017 Sudhanshu Vishnoi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*
File and object handling

Usage:
$ g++ main.cpp -o main -lboost_serialization
$ ./main
*/

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include "person-class.cpp"
#include "read-txt-write-object.cpp"
#include "read-object-write-txt.cpp"

int main() {
    char infile[100], outfile[100];

    // to read a comma separated text file
    //  and create person object for each enrty
    //  and write output to an object file
    std::cout << "Path of input (csv/txt) file: ";
    std::cin.getline(infile, sizeof(infile));
    std::cout << "Path of output (object) file: ";
    std::cin.getline(outfile, sizeof(outfile));
    read_text_write_object(infile, outfile);

    // to read a binary object file (objects being of Person class)
    //  and re-create person object for each enrty
    //  and write output to a text file
    std::cout << "Path of input (object) file: ";
    std::cin.getline(infile, sizeof(infile));
    std::cout << "Path of output (csv/txt) file: ";
    std::cin.getline(outfile, sizeof(outfile));

    read_object_write_text(infile, outfile);

    return 0;
}
