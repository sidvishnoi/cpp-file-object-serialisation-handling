/*
MIT License

Copyright (c) 2017 Sudhanshu Vishnoi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*
Module for reading a csv file and writing it's output as a binary object file

Usage:

include following for separate module testing:
#include "iostream"
#include "fstream"
#include "cstdlib"
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include "person-class.cpp"

$ g++ read-text-write-object.cpp -lboost_serialization
$ ./a.out
*/


#include "string"
#include "stack"


void read_text_write_object(char* infilename, char* outfilename) {
    /*
    Objective:  to read a comma separated text file and create person
        object for each enrty and write output to an object file
    Input parameters:
        infilename : name of input text file
        outfilename: name of output object file
    Return: None
    Error:
        if input file cannot open, "Cannot open given file. EXITING." exit(0)
    Assumption: all entries separated by a newline and each attribute in an entry
        separated by a single comma (delim)
    */
    std::string name, dob, address, line, token, delim = ",";
    std::size_t last, next;
    std::stack <std::string> tokens;

    std::ifstream infile(infilename);
    if (!infile) {
        std::cout << "Cannot open given file. EXITING.\n";
        exit(0);
    }

    std::ofstream outfile(outfilename);

    while (getline(infile, line)) {
        // split string at delimitter: http://stackoverflow.com/q/14265581
        // use stack to temporarily store the tokens
        last = 0;
        next = 0;
        while ((next = line.find(delim, last)) != std::string::npos) {
            token = line.substr(last, next-last);
            tokens.push(token);
            last = next + 1;
        }
        tokens.push(line.substr(last));

        address = tokens.top(); tokens.pop();
        dob = tokens.top(); tokens.pop();
        name = tokens.top();

        // create Person object from read data
        Person p(name, dob, address);

        // serialize and write to outfile, using boost
        boost::archive::binary_oarchive oa(outfile);
        oa << p;
    }

    infile.close();
    outfile.close();
}

// int main() {
//     char infile[100], outfile[100];
//     std::cout << "Path of input (csv/txt) file: ";
//     std::cin.getline(infile, sizeof(infile));
//     std::cout << "Path of output (object) file: ";
//     std::cin.getline(outfile, sizeof(outfile));
//     read_text_write_object(infile, outfile);
//     return 0;
// }
