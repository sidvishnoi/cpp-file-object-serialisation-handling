# File and object handling in C++

## Modules

- `person-class.cpp` *(Person class)*

- `read-txt-write-object.cpp` *(for reading a csv file and writing it's output as a binary object file)*

- `read-object-write-txt.cpp` *(for reading an object file, with objects of class Person
and write it's attributes to a text file)*

### Requirements

* Boost `sudo apt-get install libboost-all-dev` (*[http://www.boost.org/](http://www.boost.org/)*)

### Usage

Run:

```
$ g++ main.cpp -o main -lboost_serialization
$ ./main

Path of input (csv/txt) file: source.txt
Path of output (object) file: out
Path of input (object) file: out
Path of output (csv/txt) file: out.txt
```



### MIT License

Copyright (c) 2017 Sudhanshu Vishnoi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
