/*
MIT License

Copyright (c) 2017 Sudhanshu Vishnoi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*
Person class

Usage:
// to inititialize class with default attributes
Person identifier;
Person identifer (string name, string dob, string address)
// to inititialize class with given attributes
*/

class Person{
 private:
        // object serialisation for boost
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version) {
            ar & name;
            ar & dob;
            ar & address;
        }

 public:
        std::string name, dob, address;
        // constructor
        Person(std::string Name = "New Person", \
         std::string Dob = "01/01/1971", \
         std::string Address = "Earth"):
            name(Name), dob(Dob), address(Address) {}

        std::string getName() {
            // abstraction (return name)
            return name;
        }

        std::string getDob() {
            // abstraction (return address)
            return dob;
        }

        std::string getAddress() {
            // abstraction (return address)
            return address;
        }

        void print() {
            // print the object's attributes
            std::cout << "Name: " << name << std::endl << "DOB: "
            << dob << std::endl << "Address: " << address << std::endl;
        }
};
